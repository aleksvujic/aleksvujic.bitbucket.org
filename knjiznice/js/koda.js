var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrIdPacientov = [];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function generirajPodatke(stPacienta) {
	document.getElementById('izberiBolnika').selectedIndex = '0';
	document.getElementById('preberiEHRid').value = '';
	document.getElementById('ime').innerHTML = '-';
	document.getElementById('priimek').innerHTML = '-';
	document.getElementById('starost').innerHTML = '-';
	document.getElementById('visina').innerHTML = '-';
	document.getElementById('teza').innerHTML = '-';
	document.getElementById('bmi').innerHTML = '-';
	document.getElementById('myChart').style.height = 0;
	document.getElementById('myChart').style.width = 0;
	
	var rezultat, ime, priimek, birthDate;
	var zapSt = 1;
	
	// 1 - MIKE TYSON
	var sessionId = getSessionId();
	
	zapSt = 1;
	ime = "Mike";
	priimek = "Tyson";
	birthDate = "1966-6-30T21:12";
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    async: false,
	    success: function (data) {
	        var ehrId = data.ehrId;
	
	        // build party data
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: birthDate,
	            partyAdditionalInfo: [
	                {
	                    key: "ehrId",
	                    value: ehrId
	                }
	            ]
	        };
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            async: false,
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    rezultat = ehrId;
	                    ehrIdPacientov[zapSt - 1] = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].value = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].text = ime + " " + priimek;
	                    
	                    // dodaj se ostale podatke o pacientu
	                    dodajPodatke(ehrId);
	                }
	            }
	        });
	    },
	});
	
	// 2 - PETER CROUCH
	var sessionId = getSessionId();
	
	zapSt = 2;
	ime = "Peter";
	priimek = "Crouch";
	birthDate = "1981-1-30T05:32";
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    async: false,
	    success: function (data) {
	        var ehrId = data.ehrId;
	
	        // build party data
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: birthDate,
	            partyAdditionalInfo: [
	                {
	                    key: "ehrId",
	                    value: ehrId
	                }
	            ]
	        };
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            async: false,
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    rezultat = ehrId;
	                    ehrIdPacientov[zapSt - 1] = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].value = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].text = ime + " " + priimek;
	                    
	                    // dodaj se ostale podatke o pacientu
	                    dodajPodatke(ehrId);
	                }
	            }
	        });
	    }
	});
	
	// 3 - ZACH GALIFIANAKIS
	var sessionId = getSessionId();
	
	zapSt = 3;
	ime = "Zach";
	priimek = "Galifianakis";
	birthDate = "1969-10-1T05:01";
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    async: false,
	    success: function (data) {
	        var ehrId = data.ehrId;
	
	        // build party data
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: birthDate,
	            partyAdditionalInfo: [
	                {
	                    key: "ehrId",
	                    value: ehrId
	                }
	            ]
	        };
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            async: false,
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    rezultat = ehrId;
	                    ehrIdPacientov[zapSt - 1] = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].value = rezultat;
	                    document.getElementById('izberiBolnika').options[zapSt].text = ime + " " + priimek;
	                    
	            		// dodaj se ostale podatke o pacientu
	                    dodajPodatke(ehrId);
	                }
	            }
	        });
	    }
	});
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function dodajPodatke(ehrId) {
	var sessionId = getSessionId();
	
	var merilec = "Aleks Vujic";
	var visina, teza, obvestilo, message;
	if (ehrIdPacientov[0] == ehrId) {
		visina = 178;
		teza = 109;
		obvestilo = 'obvestilo1';
		message = 'message1';
	} else if (ehrIdPacientov[1] == ehrId) {
		visina = 201;
		teza = 75;
		obvestilo = 'obvestilo2';
		message = 'message2';
	} else if (ehrIdPacientov[2] == ehrId) {
		visina = 170;
		teza = 86;
		obvestilo = 'obvestilo3';
		message = 'message3';
	}
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	
	var compositionData = {
	    "ctx/time": "2014-3-19T13:10Z",
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "vital_signs/height_length/any_event/body_height_length": visina,
	    "vital_signs/body_weight/any_event/body_weight": teza
	};
	
	var queryParams = {
	    "ehrId": ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: merilec
	};
	
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(queryParams),
	    type: 'POST',
	    async: false,
	    contentType: 'application/json',
	    data: JSON.stringify(compositionData),
	    success: function (res) {
	        document.getElementById(obvestilo).style.display = 'inherit';
	        document.getElementById(message).textContent = "Uspešno ustvarjen pacient z EhrID: " + ehrId;
	    }
	});
}

function starost(dan, mesec, leto) {
    var today_date = new Date();
    var today_year = today_date.getFullYear();
    var today_month = today_date.getMonth();
    var today_day = today_date.getDate();
    var age = today_year - leto;

    if (today_month < (mesec - 1)) {
        age--;
    }
    
    if (((mesec - 1) == today_month) && (today_day < dan)) {
        age--;
    }
    return age;
}

function narisiGraf(bmi) {
	document.getElementById('myChart').width = 500;
	document.getElementById('myChart').height = 500;
	if (bmi != "") {
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["normalen BMI", "pacientov BMI"],
		        datasets: [{
		            data: [22, bmi],
		            label: [""],
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)'
		            ],
		            borderWidth: 2
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        },
		        legend: {
			        display: false
			    }
		    }
		});
	}
}

function posodobiPodatke(stevilo) {
	var sessionId = getSessionId();
	
	var ehrId;
	
	// dropdown
	if (stevilo == 1) {
		var select = document.getElementById('izberiBolnika');
		ehrId = select.options[select.selectedIndex].value;
		document.getElementById('preberiEHRid').value = ehrId;
		document.getElementById('preberiSporocilo').style.display = 'none';
		
		if (ehrId == "") {
			document.getElementById('izberiBolnika').selectedIndex = '0';
			document.getElementById('preberiEHRid').value = '';
			document.getElementById('ime').innerHTML = '-';
			document.getElementById('priimek').innerHTML = '-';
			document.getElementById('starost').innerHTML = '-';
			document.getElementById('visina').innerHTML = '-';
			document.getElementById('teza').innerHTML = '-';
			document.getElementById('bmi').innerHTML = '-';
			document.getElementById('myChart').style.height = 0;
			document.getElementById('myChart').style.width = 0;
			return;
		}
	} else {
		// rocni vnos
		ehrId = document.getElementById('preberiEHRid').value;
		document.getElementById('preberiSporocilo').style.display = 'inherit';
		if (ehrId == "") {
			$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka! Vnesi vse podatke in poskusi znova!</span>");
			return;
		}
	}
	
	var teza, visina;
	
	$.ajax({
	    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    type: 'GET',
	    async: false,
	    headers: {
	        "Ehr-Session": sessionId
	    },
	    success: function (data) {
	        var party = data.party;
	        document.getElementById('ime').innerHTML = party.firstNames;
	        document.getElementById('priimek').innerHTML = party.lastNames;
	        var datum = party.dateOfBirth.split("T")[0].split("-");
	        var dan = datum[2];
	        var mesec = datum[1];
	        var leto = datum[0];
	        document.getElementById("starost").innerHTML = starost(dan, mesec, leto) + " let";
	        document.getElementById('preberiSporocilo').style.display = 'none';
	    },
	    error: function(err) {
	    	$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	    	document.getElementById('izberiBolnika').selectedIndex = '0';
			document.getElementById('preberiEHRid').value = '';
			document.getElementById('ime').innerHTML = '-';
			document.getElementById('priimek').innerHTML = '-';
			document.getElementById('starost').innerHTML = '-';
			document.getElementById('visina').innerHTML = '-';
			document.getElementById('teza').innerHTML = '-';
			document.getElementById('bmi').innerHTML = '-';
			document.getElementById('myChart').style.height = 0;
			document.getElementById('myChart').style.width = 0;
	    	return;
	    }
	});
	
	$.ajax({
	    url: baseUrl + "/view/" + ehrId + "/weight",
	    type: 'GET',
	    async: false,
	    headers: {
	        "Ehr-Session": sessionId
	    },
	    success: function (res) {
	        for (var i in res) {
	        	document.getElementById('teza').innerHTML = res[i].weight + " kg";
	        	teza = res[i].weight;
	        }
	    }
	});
	
	$.ajax({
	    url: baseUrl + "/view/" + ehrId + "/height",
	    type: 'GET',
	    async: false,
	    headers: {
	        "Ehr-Session": sessionId
	    },
	    success: function (res) {
	        for (var i in res) {
	        	document.getElementById('visina').innerHTML = res[i].height + " cm";
	        	visina = res[i].height;
	        }
	    }
	});
	
	if (teza && visina) {
		var bmi = Math.round(teza / (visina / 100 * visina / 100));
		document.getElementById('bmi').innerHTML = bmi;
		narisiGraf(bmi);
	}
}

$(document).ready(function() {
	function initialize() {
		navigator.geolocation.getCurrentPosition(function(location) {
			var pyrmont = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
		
			var map = new google.maps.Map(document.getElementById('map'), {
				center: pyrmont,
				zoom: 12,
				scrollwheel: true
			});
			
			var request = {
				location: pyrmont,
				radius: '15000',
				types: ['park', 'gym']
			};
			
			var marker = new google.maps.Marker({
		        position: {lat: location.coords.latitude, lng: location.coords.longitude},
		        map: map,
		        animation: google.maps.Animation.DROP,
		        icon: "images/location.png"
		    });
			
			var service = new google.maps.places.PlacesService(map);
			
			service.nearbySearch(request, function(results, status) {
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					for (var i = 0; i < results.length; i++) {
						var place = results[i];
						var marker = new google.maps.Marker({
							map: map,
							position: place.geometry.location
						});
					}
				}
			});
		});
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
});